# NgAuth - Modals
> The library is currently in "beta".


The goal of this project is
to create a set of auth-related reusable components and services
as an Angular module.


Modals module source code:

* [NgAuth Modals](https://gitlab.com/ngauth/modals/tree/master/modals)
* The modals module depends on the [core](https://gitlab.com/ngauth/core), [services](https://gitlab.com/ngauth/services), and [forms](https://gitlab.com/ngauth/forms) modules.


NPM package:

* [@ngauth/modals](https://www.npmjs.com/package/@ngauth/modals)




A live _work-in-progress_ demo app is available here:
[NgAuth Cognito Demo](https://ngauth.gitlab.io/cognito-driver)


1. The library is mostly frontend/UI, and it uses AWS Cognito for serverless backend. (Instruction on how to set up backend will be provided. _tbd_)
2. It has a dependency on `amazon-cognito-identity-js` in addition to `aws-sdk` (among others).
3. NgAuth components use `@angular/material`,
and if you don't enable angular material in your app,
these components will not work properly.

For angular material setup instructions, refer to this document:

* [Getting Started with Angular Material](https://gitlab.com/angularmaterial/setup)





_tbd_


(As indicated, the library is currently in _beta_ state,
and the APIs may change over time.)



## References

The code is mostly based on the sample code
in the following three tutorial repos on GitHub:


* [awslabs/aws-cognito-angular-quickstart](https://github.com/awslabs/aws-cognito-angular-quickstart)
* [awslabs/aws-cognito-apigw-angular-auth](https://github.com/awslabs/aws-cognito-apigw-angular-auth)
* [awslabs/aws-serverless-auth-reference-app](https://github.com/awslabs/aws-serverless-auth-reference-app)




