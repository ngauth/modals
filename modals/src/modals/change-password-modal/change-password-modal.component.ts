import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { ChangePasswordComponent } from '@ngauth/forms';


@Component({
  selector: 'ngauth-modal-change-password',
  templateUrl: './change-password-modal.component.html',
  styleUrls: ['./change-password-modal.component.css']
})
export class ChangePasswordModalComponent implements OnInit, DialogParentCallback {

  @ViewChild("ngAuthChangePassword")
  ngAuthChangePassword: ChangePasswordComponent;

  constructor(
    private matSnackBar: MatSnackBar,
    private dialogRef: MatDialogRef<ChangePasswordModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit() {
    this.ngAuthChangePassword.setCallback(this);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  dialogOperationDone(action: (string | null)): void {
    if (action == 'cancel') {
      this.dialogRef.close();
    } else {
      // ignore...
    }
  }


  onPasswordChanged(cognitoResult: CognitoResult) {
    // if(isDL()) dl.log(`cognitoResult =  ${cognitoResult.toString()}`);  // ???
    if(isDL()) dl.log(`cognitoResult =  ${JSON.stringify(cognitoResult)}`);

    if (cognitoResult.message) {
      this.matSnackBar.open(cognitoResult.message,
        'Change Password',
        { duration: 2500 });
    } else {
      this.matSnackBar.open("Successfully changed",
        'Change Password',
        { duration: 2500 });
    }
    this.dialogRef.close();
  }

}
