import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { ResetPasswordStep1Component, ResetPasswordStep2Component } from '@ngauth/forms';


@Component({
  selector: 'ngauth-modal-reset-password',
  templateUrl: './reset-password-modal.component.html',
  styleUrls: ['./reset-password-modal.component.css']
})
export class ResetPasswordModalComponent implements OnInit {

  @ViewChild("ngAuthResetPasswordStep1")
  ngAuthResetPasswordStep1: ResetPasswordStep1Component;
  @ViewChild("ngAuthResetPasswordStep2")
  ngAuthResetPasswordStep2: ResetPasswordStep2Component;

  constructor(
    private matSnackBar: MatSnackBar,
    private dialogRef: MatDialogRef<ResetPasswordModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit() {
    this.ngAuthResetPasswordStep1.setCallback({
      dialogOperationDone: (action) => {
        if (action == 'cancel') {
          this.dialogRef.close();
        } else {
          // ignore...
        }
      }
    });
    this.ngAuthResetPasswordStep2.setCallback({
      dialogOperationDone: (action) => {
        if (action == 'cancel') {
          this.dialogRef.close();
        } else {
          // ignore...
        }
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onPasswordResetStarted(cognitoResult: CognitoResult) {
    // if(isDL()) dl.log(`cognitoResult =  ${cognitoResult.toString()}`);  // ???
    if(isDL()) dl.log(`cognitoResult =  ${JSON.stringify(cognitoResult)}`);

    if (cognitoResult.message) {
      this.matSnackBar.open(cognitoResult.message,
        'Reset Password',
        { duration: 2500 });

      // TBD: Go back to the first step.
      // For now, just dismiss the dialog.
      this.dialogRef.close();
    } else {
      // Continue...
      let email = this.ngAuthResetPasswordStep1.email;
      if (email) {
        // tbd: Do this only if this.ngAuthResetPasswordStep2.email is not already set ????
        this.ngAuthResetPasswordStep2.email = email;
      }
      // ...
    }
  }

  onPasswordResetConfirmed(cognitoResult: CognitoResult) {
    if(isDL()) dl.log(`cognitoResult =  ${cognitoResult.toString()}`);  // ???
    // if(isDL()) dl.log(`cognitoResult =  ${JSON.stringify(cognitoResult)}`);

    if (cognitoResult.message) {
      this.matSnackBar.open(cognitoResult.message,
        'Reset Password',
        { duration: 2500 });
    } else {
      this.matSnackBar.open("Successfully reset",
        'Reset Password',
        { duration: 2500 });
    }
    this.dialogRef.close();
  }

}
